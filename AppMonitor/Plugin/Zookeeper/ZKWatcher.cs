﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooKeeperNet;

namespace AppMonitor.Plugin.Zookeeper
{
    public class ZKWatcher : IWatcher
    {
        public void Process(WatchedEvent @event)
        {
            Console.WriteLine("-->" + @event.Path + "---" + @event.State + "---" + @event.Type + "---" + @event.Wrapper);
        }


    }
}
